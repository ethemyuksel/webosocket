package tr.com.ethem.model;

public class Stock {
	private int id;
	private String val;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	public Stock(int id, String val) {
		super();
		this.id = id;
		this.val = val;
	}
}
