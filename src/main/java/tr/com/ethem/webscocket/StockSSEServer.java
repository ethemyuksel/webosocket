package tr.com.ethem.webscocket;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.ServerEndpoint;

import tr.com.ethem.rabbitmq.Recv;

@WebServlet(urlPatterns = "/stocksseendpoint", loadOnStartup = 1)
public class StockSSEServer extends HttpServlet {

	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("line19");
	}

	@Override
	public void init() throws ServletException {
		System.out.println("line24");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("doGet...");
		execute(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
		System.out.println("doPost...");
		execute(req, resp);
	}

	public void execute(HttpServletRequest req, HttpServletResponse resp) {
		resp.setContentType("text/event-stream");
		try {
			PrintWriter pw = resp.getWriter();
			
			Recv rec = new Recv(req, resp);
			rec.listen();
			//pw.write("event: server-time\n\n"); // take note of the 2 \n 's, also on the next line.
			//pw.write("data: " + messagesSent + "\n\n");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
