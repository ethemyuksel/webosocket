package tr.com.ethem.webscocket;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import tr.com.ethem.rabbitmq.Recv;

@ServerEndpoint("/stockwsendpoint")
public class StockWsServer {

	@OnOpen
	public void onOpen(Session session) {
		try {
			new Recv(session).listen();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@OnClose
	public void onClose() {
		System.out.println("Close Connection ...");
	}

	@OnMessage
	public String onMessage(String message) {
		System.out.println("Message from the client: " + message);
		String echoMsg = "Echo from the server : " + message;
		return echoMsg;
	}

	public void onError(Throwable e) {
		e.printStackTrace();
	}

}
