package tr.com.ethem.webscocket;

import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocketendpoint")
public class WsServer {

	Session session = null;

	@OnOpen
	public void onOpen(Session session) {
		this.session = session;
		try {
			session.getBasicRemote().sendText("You are connected. Your ID is " + session.getId());
			
			Runnable r = () -> {
				int i = 1;
				while (true) {
					try {
						Thread.sleep(5000);
						System.out.println("i = " + i);
						onMessageCust("" + i);
						i++;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
			new Thread(r).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@OnClose
	public void onClose() {
		System.out.println("Close Connection ...");
	}

	@OnMessage
	public String onMessage(String message) {
		// System.out.println("Message from the client: " + message);
		String echoMsg = "Echo from the server : " + message;
		return echoMsg;
	}
	
	public void onMessageCust(String message) {
		try {
			session.getBasicRemote().sendText(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onError(Throwable e) {
		e.printStackTrace();
	}

}
