package tr.com.ethem.rabbitmq;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import tr.com.ethem.model.Stock;

public class Send {

    private final static String QUEUE_NAME = "stock";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
       // try (Connection connection = factory.newConnection();
         //    Channel channel = connection.createChannel()) {
        	Connection connection = factory.newConnection();
        	Channel channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            channel.basicPublish("", QUEUE_NAME, null, "hello from server".getBytes(StandardCharsets.UTF_8));
            System.out.println(" [x] Sent 'hello '");
            
            Runnable r = () -> {
            	int value1 = 70;
            	int value2 = 100;
            	int value3 = 50;
            	while(true) {
            		try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		Stock st1 = new Stock(1, value1 + "$");
            		Stock st2 = new Stock(2, value2 + "$");
            		Stock st3 = new Stock(3, value3 + "$");
            		List<Stock> list = Arrays.asList(st1, st2, st3);
            		
            		ObjectMapper objectMapper = new ObjectMapper();
            		ByteArrayOutputStream baos = new ByteArrayOutputStream();
            		try {
						objectMapper.writeValue(baos, list);
					} catch (StreamWriteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DatabindException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		String message = new String(baos.toByteArray());
                    try {
						channel.basicPublish("", QUEUE_NAME, null, baos.toByteArray());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    System.out.println(" [x] Sent '" + message + "'");
                    value1++;
                    value2++;
                    value3++;
            	}//end of while
            };
            
            new Thread(r).start();
            
        //}//end of try
    }
}