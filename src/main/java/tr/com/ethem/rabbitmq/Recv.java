package tr.com.ethem.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

public class Recv {

    private final static String QUEUE_NAME = "stock";
	private Session session;

	HttpServletRequest req = null;
	HttpServletResponse resp = null;
	int mode = 1;
    public Recv(Session sess) {
    	this.session = sess;
    }
    
    public Recv(HttpServletRequest req, HttpServletResponse resp) {
    	this.req = req;
    	this.resp = resp;
    	mode = 2;
    }
    public static void main(String[] argv) throws Exception {
        //new Recv().listen();
    }
    
    public void listen() throws Exception {
    	ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + message + "'");
            onMessage(message);
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
    
    public void onMessage(String mesg) {
    	try {
    		if(mode == 1) {
    			session.getBasicRemote().sendText(mesg);
    		} else {
    			resp.setContentType("text/event-stream");
    			
    			resp.getWriter().write("event: server-time\n\n"); // take note of the 2 \n 's, also on the next line.
    			resp.getWriter().write("data: " + mesg + "\n\n");

    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}